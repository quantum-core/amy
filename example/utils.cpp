#include "utils.hpp"

#include <cstdlib>
#include <iostream>
#include <iterator>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

AMY_ASIO_NS::ip::tcp::endpoint global_options::tcp_endpoint() const {
    using namespace AMY_ASIO_NS::ip;

    return host.empty() ?
        tcp::endpoint(address_v4::loopback(), port) :
        tcp::endpoint(address::from_string(host), port);
}

amy::auth_info global_options::auth_info() const {
    return amy::auth_info(user, password);
}
std::vector<std::string> unrecognizedOptions;

void parse_command_line_options(int argc, char* argv[]) {
    po::options_description desc("Available options");
    desc.add_options()("help,h", "Show this help message")(
        "host,H", po::value<std::string>()->default_value("127.0.0.1"),
        "MySQL server host.")("port,P",
                              po::value<uint16_t>()->default_value(3306),
                              "MySQL server port.")(
        "user,u", po::value<std::string>()->default_value("amy"),
        "Login user.")("password,p",
                       po::value<std::string>()->default_value("amy"),
                       "Login password.")(
        "schema,s", po::value<std::string>()->default_value("test_amy"),
        "Default schema to use.");

     po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(argc, argv)
                                    .options(desc)
                                    .allow_unregistered()
                                    .run();
    po::store(parsed, vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        exit(EXIT_SUCCESS);
    }

    opts.host = vm["host"].as<std::string>();
    opts.port = vm["port"].as<uint16_t>();
    opts.user = vm["user"].as<std::string>();
    opts.password = vm["password"].as<std::string>();
    opts.schema = vm["schema"].as<std::string>();

    unrecognizedOptions =
        po::collect_unrecognized(parsed.options, po::include_positional);

    /*for (auto it : unrecognizedOptions) {
        std::cout << "Unrecognized option: " << it << "\n";
    }*/
}

std::string read_from_stdin() {
    return unrecognizedOptions[0];
}

void check_error(AMY_SYSTEM_NS::error_code const& ec) {
    if (ec) {
        throw AMY_SYSTEM_NS::system_error(ec);
    }
}

void report_system_error(AMY_SYSTEM_NS::system_error const& e) {
    std::cerr
        << "System error: "
        << e.code().value() << " - " << e.what()
        << std::endl;
}

// vim:ft=cpp sw=4 ts=4 tw=80 et
